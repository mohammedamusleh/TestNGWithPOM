package script;

import com.github.javafaker.Faker;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import pages.UnitedAirlinesPages.UnitedAirlinesHome;
import utilities.Driver;

public class Base {

    WebDriver driver;
    UnitedAirlinesHome unitedAirlinesHome;


    @BeforeMethod
    public void setUp(){
        driver = Driver.getDriver();
        unitedAirlinesHome = new UnitedAirlinesHome(driver);
    }

    @AfterMethod
    public void tearDown(){
        Driver.quitDriver();
    }
}