package script;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.UnitedAirlinesPages.UnitedAirlinesHome;
import utilities.ConfigReader;
import utilities.Waiter;

import java.util.List;

public class UnitedAirlinesTest extends Base {

    @Test(testName = "Validate Main menu navigation items", priority = 1)
    public void validateMainMenuNavigationItems() {
        driver.get(ConfigReader.getProperty("unitedAirlinesURL"));

        for (int i = 0; i < unitedAirlinesHome.menuOptions.size(); i++) {
            Assert.assertEquals(unitedAirlinesHome.menuOptions.get(i).getText(), ExpectedResults.mainMenuNavi[i]);
        }
    }

    @Test(testName = "Validate Book travel menu navigation items", priority = 2)
    public void validateBookTravelMenuItems() {
        driver.get(ConfigReader.getProperty("unitedAirlinesURL"));
        for (int i = 0; i < unitedAirlinesHome.bookTravelMenu.size(); i++) {
            Assert.assertEquals(unitedAirlinesHome.bookTravelMenu.get(i).getText(), ExpectedResults.bookTravelMenu[i]);
        }
    }

    @Test(testName = "Validate Round-trip and One-way radio buttons", priority = 3)
    public void validateRoundTripAndOneWayRadioButtons() {
        driver.get(ConfigReader.getProperty("unitedAirlinesURL"));

        Assert.assertTrue(unitedAirlinesHome.roundTripButtonLabel.isDisplayed());
        Assert.assertTrue(unitedAirlinesHome.roundTripButtonInput.isEnabled());
        Assert.assertTrue(unitedAirlinesHome.roundTripButtonInput.isSelected());

        Assert.assertTrue(unitedAirlinesHome.oneWayTripButtonLabel.isDisplayed());
        Assert.assertTrue(unitedAirlinesHome.oneWayTripButtonInput.isEnabled());
        Assert.assertFalse(unitedAirlinesHome.oneWayTripButtonInput.isSelected());
    }

    @Test(testName = "Validate Book with miles and Flexible dates checkboxes", priority = 4)
    public void validateBookWithMilesAndFlexibleDatesCheckboxes() throws InterruptedException {
        driver.get(ConfigReader.getProperty("unitedAirlinesURL"));

        Assert.assertTrue(unitedAirlinesHome.bookWithMilesLabel.isDisplayed());
        Assert.assertTrue(unitedAirlinesHome.bookWithMilesInput.isEnabled());
        Assert.assertFalse(unitedAirlinesHome.bookWithMilesInput.isSelected());

        Assert.assertTrue(unitedAirlinesHome.flexibleDatesLabel.isDisplayed());
        Assert.assertTrue(unitedAirlinesHome.flexibleDatesInput.isEnabled());
        Assert.assertFalse(unitedAirlinesHome.flexibleDatesInput.isSelected());

        unitedAirlinesHome.bookWithMilesLabel.click();
        unitedAirlinesHome.flexibleDatesLabel.click();

        Assert.assertTrue(unitedAirlinesHome.bookWithMilesInput.isSelected());
        Assert.assertTrue(unitedAirlinesHome.flexibleDatesInput.isSelected());

        unitedAirlinesHome.bookWithMilesLabel.click();
        unitedAirlinesHome.flexibleDatesLabel.click();

        Assert.assertFalse(unitedAirlinesHome.bookWithMilesInput.isSelected());
        Assert.assertFalse(unitedAirlinesHome.flexibleDatesInput.isSelected());
    }

    @Test(testName = "Validate One-way ticket search results from Chicago, IL, US (ORD) toMiami, FL, US (MIA)",
            priority = 5)
    public void validateOneWayTicketSearchResult() throws InterruptedException {
        driver.get(ConfigReader.getProperty("unitedAirlinesURL"));
        unitedAirlinesHome.oneWayButton.click();

        unitedAirlinesHome.fromInputBoxChicago.clear();
        unitedAirlinesHome.fromInputBoxChicago.sendKeys("Chicago, IL, US (ORD)");

        unitedAirlinesHome.toInputBoxMiami.clear();
        unitedAirlinesHome.toInputBoxMiami.sendKeys("Miami, FL, US (MIA)");

        unitedAirlinesHome.datePicker.click();
        unitedAirlinesHome.leftArrowCalendar.click();
        unitedAirlinesHome.leftArrowCalendar.click();
        unitedAirlinesHome.jan30Date.click();

        unitedAirlinesHome.passengers.click();
        unitedAirlinesHome.adults.click();

       unitedAirlinesHome.cabinTypeButton.click();
       unitedAirlinesHome.cabinTypeSelect.click();

       unitedAirlinesHome.findFlightButton.click();

        Assert.assertEquals(driver.findElement(By.xpath("//div[contains(@class,'tripOriginDestinationHeader')]")).getText(),
                ExpectedResults.departureMessage);

    }
}