package script;

public class ExpectedResults {
   public static final String[] mainMenuNavi = {"BOOK", "MY TRIPS", "TRAVEL INFO", "MILEAGEPLUS® PROGRAM", "DEALS", "HELP"};
   public static final String[] bookTravelMenu = {"Book", "Flight status", "Check-in", "My trips"};
   public static final String departureMessage = "Depart: Chicago, IL, US to Miami, FL, US";
}
