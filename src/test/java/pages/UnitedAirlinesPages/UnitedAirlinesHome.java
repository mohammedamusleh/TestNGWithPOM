package pages.UnitedAirlinesPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class UnitedAirlinesHome {
    public UnitedAirlinesHome(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    //Task 1
    @FindBy(xpath = "//div[@role='tablist']//span]")
    public List<WebElement> menuOptions;

    //Task 2
    @FindBy(xpath = "(//ul[@role='tablist'])[1]/li")
    public List<WebElement> bookTravelMenu;

    //Task 3
    @FindBy(xpath = "//label[@for='roundtrip']")
    public WebElement roundTripButtonLabel;

    @FindBy(id = "roundtrip")
    public WebElement roundTripButtonInput;

    @FindBy(xpath = "//label[@for='oneway']")
    public WebElement oneWayTripButtonLabel;

    @FindBy(id = "oneway")
    public WebElement oneWayTripButtonInput;

    //Task 4
    @FindBy(xpath = "//label[@for='award']")
    public WebElement bookWithMilesLabel;

    @FindBy(id = "award")
    public WebElement bookWithMilesInput;

    @FindBy(xpath = "//label[@for='flexibleDates']")
    public WebElement flexibleDatesLabel;

    @FindBy(id = "flexibleDates")
    public WebElement flexibleDatesInput;

    //Task 5
    @FindBy(xpath = "//label[@for='oneway']")
    public WebElement oneWayButton;

    @FindBy(id = "bookFlightOriginInput")
    public WebElement fromInputBoxChicago;

    @FindBy(id = "bookFlightDestinationInput")
    public WebElement toInputBoxMiami;

    @FindBy(xpath = "//div[@class='SingleDatePicker SingleDatePicker_1']")
    public WebElement datePicker;

    @FindBy(xpath = "//span[@class='app-containers-BookCalendar-bookCalendar__prevIcon--2LvFf']")
    public WebElement leftArrowCalendar;

    @FindBy(xpath = "(//td[@class='CalendarDay CalendarDay_1 CalendarDay__default CalendarDay__default_2 CalendarDay__firstDayOfWeek CalendarDay__firstDayOfWeek_3'])[1]")
    public WebElement jan30Date;

    @FindBy(id = "passengerSelector")
    public WebElement passengers;

    @FindBy(xpath = "//button[@aria-label='Substract one Adult']")
    public WebElement adults;

    @FindBy(id = "cabinType")
    public WebElement cabinTypeButton;

    @FindBy(xpath = "//li[@aria-label='Business or First']")
    public WebElement cabinTypeSelect;

    @FindBy(xpath = "//button[@aria-label='Find flights']")
    public WebElement findFlightButton;
}
